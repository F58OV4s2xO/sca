import java.security.Key;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Security;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Base64;

import javax.crypto.Cipher;

import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.pkcs.RSAPrivateKey;
import org.bouncycastle.asn1.pkcs.RSAPublicKey;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.*;
import java.security.*;
import java.security.spec.*;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.jce.provider.*;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;

public class PKI_Task1 {
	public PKI_Task1() {

	}

	public static void main(String[] args) throws Exception {
		// Task 1
		String args2[] = {
				"C:\\Users\\Simulac\\Desktop\\Eclipse\\Workspace\\PKI\\src\\enc_publicKey.pem",
				"two" };

		String s = "martinronai;ronai@protonmail.ch;2980908";
		System.out.println("s: " + s);
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		byte[] sbytearray = s.getBytes(StandardCharsets.UTF_8); // Java 7+ only
		md.update(sbytearray);
		byte[] digest = md.digest();
		String hex = String.format("%064x", new BigInteger(1, digest));
		System.out.println("hash: " + hex);
		String result = new String(Base64.getEncoder().encode((digest)));
		System.out.println("hash(sha256(s)): " + result);

		String s2 = s + ";" + result;
		System.out.println("s2: " + s2);

		String filename = "C:\\Users\\Simulac\\Desktop\\Eclipse\\Workspace\\PKI\\src\\enc_publicKey.pem";

		Key k = dwimKey(filename);
		System.out.println(k);

		// String encrypted = encrypt(s2, getPublicKeyFromString(filename));
		// System.out.println(encrypted);

		// RSAEncryption rsaenc = new RSAEncryption();
		// System.out.println();
		// rsaenc.main(args2);
		
		String c = encrypt(s2, (PublicKey) k);
		System.out.println("c: " + c);


	}

	/**
	 * Encrypt a text using public key.
	 * 
	 * @param text
	 *            The original unesnscrypted text
	 * @param key
	 *            The public key
	 * @return Encrypted text
	 * @throws java.lang.Exception
	 */

	public static String encrypt(String rawText, PublicKey publicKey)
			throws IOException, GeneralSecurityException {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		return Base64.getEncoder().encodeToString(
				(cipher.doFinal(rawText.getBytes("UTF-8"))));
	}
	

	
	public static Key dwimKey(String v) {
		File f = new File(v);
		if (f.isFile()) {
			try (PEMParser pem = new PEMParser(Files.newBufferedReader(
					f.toPath(), StandardCharsets.UTF_8))) {
				Object x = pem.readObject();
				System.out.println(x.getClass().getName());
				if (x instanceof SubjectPublicKeyInfo) {
					SubjectPublicKeyInfo pubkey = (SubjectPublicKeyInfo) x;
					// Make a private key
					return new JcaPEMKeyConverter().getPublicKey(pubkey);
				} else {
					System.out.println("-key only supports plain PEM keypairs");
					return null; // Static code analyzer
				}
			} catch (IOException e) {
				throw new IllegalArgumentException("Could not parse key: "
						+ e.getMessage(), e);
			}
		}
		return null;

	}
	
	
	public static PublicKey dwimKeyPub(String v) {
		File f = new File(v);
		if (f.isFile()) {
			try (PEMParser pem = new PEMParser(Files.newBufferedReader(
					f.toPath(), StandardCharsets.UTF_8))) {
				Object x = pem.readObject();
				System.out.println(x.getClass().getName());
				if (x instanceof SubjectPublicKeyInfo) {
					SubjectPublicKeyInfo pubkey = (SubjectPublicKeyInfo) x;
					// Make a private key
				} else {
					System.out.println("-key only supports plain PEM keypairs");
					return null; // Static code analyzer
				}
			} catch (IOException e) {
				throw new IllegalArgumentException("Could not parse key: "
						+ e.getMessage(), e);
			}
		}
		return null;

	}
}
